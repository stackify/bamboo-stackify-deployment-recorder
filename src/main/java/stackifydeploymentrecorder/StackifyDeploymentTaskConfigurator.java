package stackifydeploymentrecorder;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.opensymphony.xwork.TextProvider;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class StackifyDeploymentTaskConfigurator extends AbstractTaskConfigurator
{
    private TextProvider textProvider;

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);

        config.put("apiKey", params.getString("apiKey"));
        config.put("app", params.getString("app"));
        config.put("env", params.getString("env"));
        config.put("version", params.getString("version"));
        config.put("name", params.getString("name"));
        config.put("action", params.getString("action"));
        config.put("branch", params.getString("branch"));
        config.put("commit", params.getString("commit"));
        config.put("uri", params.getString("uri"));

        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);

        context.put("apiKey", "${bamboo.Stackify.ApiKey}");
        context.put("app", "");
        context.put("env", "${bamboo.deploy.environment}");
        context.put("version", "${bamboo.deploy.release}");
        context.put("name", "${bamboo.buildKey}");
        context.put("action", "complete");
        context.put("branch", "${bamboo.planRepository.branch}");
        context.put("commit", "${bamboo.planRepository.revision}");
        context.put("uri", "${bamboo.resultsUrl}");
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);

        context.put("apiKey", taskDefinition.getConfiguration().get("apiKey"));
        context.put("app", taskDefinition.getConfiguration().get("app"));
        context.put("env", taskDefinition.getConfiguration().get("env"));
        context.put("version", taskDefinition.getConfiguration().get("version"));
        context.put("name", taskDefinition.getConfiguration().get("name"));
        context.put("action", taskDefinition.getConfiguration().get("action"));
        context.put("branch", taskDefinition.getConfiguration().get("branch"));
        context.put("commit", taskDefinition.getConfiguration().get("commit"));
        context.put("uri", taskDefinition.getConfiguration().get("uri"));
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        context.put("apiKey", taskDefinition.getConfiguration().get("apiKey"));
        context.put("app", taskDefinition.getConfiguration().get("app"));
        context.put("env", taskDefinition.getConfiguration().get("env"));
        context.put("version", taskDefinition.getConfiguration().get("version"));
        context.put("name", taskDefinition.getConfiguration().get("name"));
        context.put("action", taskDefinition.getConfiguration().get("action"));
        context.put("branch", taskDefinition.getConfiguration().get("branch"));
        context.put("commit", taskDefinition.getConfiguration().get("commit"));
        context.put("uri", taskDefinition.getConfiguration().get("uri"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        final String apiKeyValue = params.getString("apiKey");
        if (StringUtils.isEmpty(apiKeyValue))
        {
            errorCollection.addError("apiKey", "API Key can not be empty");
        }

        final String appValue = params.getString("app");
        if (StringUtils.isEmpty(appValue))
        {
            errorCollection.addError("app", "App Name can not be empty");
        }

        final String envValue = params.getString("env");
        if (StringUtils.isEmpty(envValue))
        {
            errorCollection.addError("env", "Environment can not be empty");
        }

        final String versionValue = params.getString("version");
        if (StringUtils.isEmpty(versionValue))
        {
            errorCollection.addError("version", "Version can not be empty");
        }

        final String actionValue = params.getString("action");
        if (StringUtils.isEmpty(actionValue))
        {
            errorCollection.addError("action", "Deployment Action can not be empty");
        }
    }

    public void setTextProvider(final TextProvider textProvider)
    {
        this.textProvider = textProvider;
    }
}