package stackifydeploymentrecorder;

import java.io.*;
import java.net.*;
import java.util.*;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.CommonTaskType;
import org.jetbrains.annotations.NotNull;

public class StackifyDeploymentTask implements CommonTaskType
{
    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final CommonTaskContext taskContext) throws TaskException
    {
        final BuildLogger buildLogger = taskContext.getBuildLogger();

        final String apiKey = taskContext.getConfigurationMap().get("apiKey");
        final String app = taskContext.getConfigurationMap().get("app");
        final String env = taskContext.getConfigurationMap().get("env");
        final String version = taskContext.getConfigurationMap().get("version");
        final String name = taskContext.getConfigurationMap().get("name");
        final String action = taskContext.getConfigurationMap().get("action");
        final String branch = taskContext.getConfigurationMap().get("branch");
        final String commit = taskContext.getConfigurationMap().get("commit");
        final String uri = taskContext.getConfigurationMap().get("uri");

        buildLogger.addBuildLogEntry("Api Key: " + apiKey);
        buildLogger.addBuildLogEntry("App Name: " + app);
        buildLogger.addBuildLogEntry("Environment: " + env);
        buildLogger.addBuildLogEntry("Release Version: " + version);
        buildLogger.addBuildLogEntry("Name: " + name);
        buildLogger.addBuildLogEntry("Deployment Action: " + action);
        buildLogger.addBuildLogEntry("Branch: " + branch);
        buildLogger.addBuildLogEntry("Commit: " + commit);
        buildLogger.addBuildLogEntry("Uri: " + uri);

        try {  
        URL url = new URL("https://api.stackify.net/api/v1/deployments/" + action);
        
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("Name", name);
        params.put("AppName", app);
        params.put("Version", version);
        params.put("EnvironmentName", env);
        params.put("Name", name);
        params.put("Branch", branch);
        params.put("Commit", commit);
        params.put("Uri", uri);

        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Authorization", "ApiKey " + apiKey);
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

        for (int c; (c = in.read()) >= 0;)
            System.out.print((char)c);            
            buildLogger.addBuildLogEntry("Deployment successfully recorded in Stackify");

        } catch (Exception e) {
            buildLogger.addBuildLogEntry("Failed to record deployment in Stackify");
            buildLogger.addBuildLogEntry(e.getMessage());
        }

        return TaskResultBuilder.newBuilder(taskContext).success().build();
    }
}