package ut.stackifydeploymentrecorder;

import org.junit.Test;
import stackifydeploymentrecorder.api.MyPluginComponent;
import stackifydeploymentrecorder.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}